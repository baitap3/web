<!-- https://viblo.asia/p/validate-du-lieu-form-don-gian-hieu-qua-hon-voi-jquery-validation-XL6lAkwJKek -->
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="./a.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
</head>


<body id="body">
    <p id="alert" style="padding-left: 2.5cm; margin-bottom : 0;color : red;"></p>
    <form action="a.php" method="post">

        <div class="container">
            <fieldset class="group_name">
                <label id="name" for="uname">Họ và tên</label>
                <input id="name_input" type="text" name="uname" >
            </fieldset>

            <?php
            $gender = array(0 => "Nam", 1 => "Nữ");
            $department = array(
                "nothing" => "Chọn phân khoa", "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học vật liệu"
            );
            ?>

            <fieldset class="group_gender">
                <label id="gender" for="gen">Giới tính</label>
                <?php
                // foreach($gender as $key => $value):
                // echo '<option value="'.$key.'">'.$value.'</option>'; //close your tags!!
                // endforeach;
                for ($x = 0; $x < 2; $x++) {
                    echo '<input class = "gender_option" type="radio" name="gen" value="' . $x . '"> ' . $gender[$x];
                }
                ?>
            </fieldset>

            <fieldset class="group_department">
                <label id="department" for="depart">Phân Khoa</label>
                <select id="select_depart" name="depart">
                    <?php
                    foreach ($department as $key => $value) :
                        if ($key == "nothing") :
                            echo '<option selected="selected" value="' . $key . '">' . $value . '</option>';
                            continue;
                        else :
                            echo $key;
                            echo '<option value="' . $key . '">' . $value . '</option>'; //close your tags!!
                        endif;
                    endforeach;
                    ?>
                </select>
            </fieldset>
            <?php
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $day = date("Y-m-d");
            ?>
            <fieldset class="date_group">
                <label id="date" for="d">Ngày sinh</label>
                <input id="date_input" placeholder="Enter Username" data-date="" data-date-format="DD/MM/YYYY" type="date" name="d" max=<?php echo $day ?>>
            </fieldset>

            <fieldset class="address_group">
                <label id="add_label" for="add">Địa chỉ</label>
                <!-- <input id="add_input" type="textarea" name="add" >
                 -->
                <textarea id="add_input" rows="5" cols="42" name="add" wrap="hard"></textarea>
            </fieldset>

            <fieldset class="register">
                <button id="my_button" type="submit">Đăng ký</button>

                <!-- <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label> -->
            </fieldset>
        </div>

        <!-- <div class="container" style="background-color:#f1f1f1">
  <button type="button" class="cancelbtn">Cancel</button>
  <span class="psw">Forgot <a href="#">password?</a></span>
</div> -->
    </form>
    <!-- <p id="test">asdas</p> -->
    <script type="text/javascript" src="./a.js"></script>
</body>

</html>